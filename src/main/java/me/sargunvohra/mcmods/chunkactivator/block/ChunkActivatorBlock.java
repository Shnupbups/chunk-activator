package me.sargunvohra.mcmods.chunkactivator.block;

import net.fabricmc.fabric.api.block.FabricBlockSettings;
import net.minecraft.block.*;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.world.BlockView;

public class ChunkActivatorBlock extends BlockWithEntity {
    public ChunkActivatorBlock() {
        super(
            FabricBlockSettings.of(Material.METAL, MaterialColor.GOLD)
                .strength(3f, 6f)
                .sounds(BlockSoundGroup.METAL)
                .build());
    }

    @Override
    public BlockEntity createBlockEntity(BlockView blockView) {
        return new ChunkActivatorBlockEntity();
    }

    @Override
    public BlockRenderType getRenderType(BlockState blockState_1) {
        return BlockRenderType.MODEL;
    }
}
